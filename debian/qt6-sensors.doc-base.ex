Document: qt6-sensors
Title: Debian qt6-sensors Manual
Author: <insert document author here>
Abstract: This manual describes what qt6-sensors is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/qt6-sensors/qt6-sensors.sgml.gz

Format: postscript
Files: /usr/share/doc/qt6-sensors/qt6-sensors.ps.gz

Format: text
Files: /usr/share/doc/qt6-sensors/qt6-sensors.text.gz

Format: HTML
Index: /usr/share/doc/qt6-sensors/html/index.html
Files: /usr/share/doc/qt6-sensors/html/*.html
